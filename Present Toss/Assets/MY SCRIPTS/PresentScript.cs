﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PresentScript : MonoBehaviour
{
    public Rigidbody rb;
    public float force;
    public bool Thrown = false;
    private float WindSpeed;
    private bool Hit;

    public GameObject ExplosionEffect;

    private void Start()
    {
        Hit = false;
        rb = GetComponent<Rigidbody>();
        GetComponent<ConstantForce>().enabled = false;

        Quaternion rot = new Quaternion();
        rot.z = 0f;
        rot.y = 0f;
        rot.x = 0f;
        GameObject.Find("Direction").transform.rotation = rot;

        WindSpeed = Random.Range(-200f, 200f);
        Vector3 force = new Vector3(WindSpeed, 0f, 0f);
        GetComponent<ConstantForce>().force = force;

        if (WindSpeed < 0)
        {
            WindSpeed *= -1;
            if (GameObject.Find("Canvas/Details/WindDirectionText"))
                GameObject.Find("Canvas/Details/WindDirectionText").GetComponent<Text>().text = "Wind Direction: Left";
            if (GameObject.Find("Canvas/Details/WindSpeedText"))
                GameObject.Find("Canvas/Details/WindSpeedText").GetComponent<Text>().text = "Wind Speed: " + Mathf.Round(WindSpeed * 50f) / 100f + " m/s";
        }
        else
        {
            if (GameObject.Find("Canvas/Details/WindDirectionText"))
                GameObject.Find("Canvas/Details/WindDirectionText").GetComponent<Text>().text = "Wind Direction: Right";
            if (GameObject.Find("Canvas/Details/WindSpeedText"))
                GameObject.Find("Canvas/Details/WindSpeedText").GetComponent<Text>().text = "Wind Speed: " + Mathf.Round(WindSpeed * 50f) / 100f + " m/s";
        }
    }

    private void ProcessInput()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !Thrown)// && !GameObject.Find("Canvas").GetComponent<PauseMenuScript>().Paused)
        {
            rb.useGravity = true;
            GetComponent<ConstantForce>().enabled = true;
            Thrown = true;
            float zPos = GameObject.Find("Horn").transform.position.z;
            float xForce = GameObject.Find("Direction").transform.rotation.z;
            float yForce = 2000 + (300 / 10f) * (zPos + 3f);
            float zForce = 5000 + (5000 / 14f) * (zPos + 3f);
            rb.AddForce((-1) * xForce * 10000, yForce, zForce);
        }
    }

    IEnumerator Destroy()
    {
        Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        GameObject.Find("PLAYER").GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(1.85f);
        Destroy(GameObject.Find(ExplosionEffect.name + "(Clone)"));
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<MeshRenderer>().sharedMaterial.name != "wall-846015" && collision.gameObject.name != "Platform")
        {
            if (!Hit)
            {
                Hit = true;
                GetComponent<ConstantForce>().enabled = false;
                GameObject.Find("PLAYER").GetComponent<PlayerScript>().multiplier = 1;
                GameObject.Find("PLAYER").GetComponent<PlayerScript>().nextMultiplierScore = 2;
                GameObject.Find("PLAYER").GetComponent<PlayerScript>().currentScore = 0;
                if (GameObject.Find("PLAYER").GetComponent<PlayerScript>().HighScore <= GameObject.Find("PLAYER").GetComponent<PlayerScript>().score)
                    GameObject.Find("PLAYER").GetComponent<PlayerScript>().HighScore = GameObject.Find("PLAYER").GetComponent<PlayerScript>().score;
                GameObject.Find("PLAYER").GetComponent<PlayerScript>().score = 0;
                Destroy(gameObject);
            }
        }
        else
        {
            GetComponent<ConstantForce>().enabled = false;
            if (collision.gameObject.name == "HornGround")
            {
                if (!Hit)
                {
                    Hit = true;
                    GameObject.Find("PLAYER").GetComponent<PlayerScript>().score += GameObject.Find("PLAYER").GetComponent<PlayerScript>().multiplier;
                    GameObject.Find("PLAYER").GetComponent<PlayerScript>().currentScore++;
                    StartCoroutine(Destroy());
                }
            }
        }
    }

    private void FixedUpdate()
    {
        ProcessInput();
    }
}
