﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuScript : MonoBehaviour
{
    public bool Paused = false;
    public GameObject PauseMenuUI;
    
    public AudioSource Music;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!Paused) Pause();
            else Resume();
        }
    }

    public void Pause()
    {
        PauseMenuUI.SetActive(true);
        Paused = true;
        Music.volume = 100;
    }
    
    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Paused = false;
        Music.volume = 100;
    }

    public void QuitButton_Click()
    {
        Application.Quit();
    }
}
