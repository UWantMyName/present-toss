﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour
{
    public GameObject DetailsUI;
    public GameObject Player;

    public void LetsPlayClick()
    {
        DetailsUI.SetActive(true);
        Player.SetActive(true);
        gameObject.SetActive(false);
    }
}
