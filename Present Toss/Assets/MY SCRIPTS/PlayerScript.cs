﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public GameObject Present1Prefab;
    public GameObject Present2Prefab;
    public GameObject Present3Prefab;
    public GameObject Present4Prefab;
    public GameObject Present5Prefab;
    private GameObject Present;

    public int score;
    public int multiplier;
    public int HighScore;
    private float updateHeight;

    public int nextMultiplierScore;
    public int currentScore;

    public GameObject ScoreText;
    public GameObject HighscoreText;
    public GameObject MultiplierText;
    public GameObject MultiplierMeter;
    public GameObject Direction;
    public AudioSource sound;

    public GameObject HornPrefab;

    private void Start()
    {
        int x = Random.Range(0, 100);
        if (x % 5 == 0) Present = Present1Prefab;
        else if (x % 5 == 1) Present = Present2Prefab;
        else if (x % 5 == 2) Present = Present3Prefab;
        else if (x % 5 == 3) Present = Present4Prefab;
        else if (x % 5 == 4) Present = Present5Prefab;

        HighScore = 0;
        multiplier = 1;
        nextMultiplierScore = 2;
        currentScore = 0;
        Instantiate(Present, new Vector3(0, 7.5f, -12), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText.GetComponent<Text>().text = "Score: " + score;
        HighscoreText.GetComponent<Text>().text = "Highscore: " + HighScore;

        Vector2 size = MultiplierMeter.GetComponent<RectTransform>().sizeDelta;
        size.y = 126 * currentScore / nextMultiplierScore;

        if (multiplier == 1) MultiplierMeter.GetComponent<Image>().color = new Color32(27, 253, 0, 255);
        else if (multiplier == 2) MultiplierMeter.GetComponent<Image>().color = new Color32(242, 253, 0, 255);
        else if (multiplier == 3) MultiplierMeter.GetComponent<Image>().color = new Color32(253, 67, 0, 255);
        else if (multiplier == 4) MultiplierMeter.GetComponent<Image>().color = new Color32(0, 1, 253, 255);
        else if (multiplier == 5) MultiplierMeter.GetComponent<Image>().color = new Color32(0, 249, 253, 255);

        if (multiplier < 5) MultiplierMeter.GetComponent<RectTransform>().sizeDelta = size;
        else
        {
            size.y = 126;
            MultiplierMeter.GetComponent<RectTransform>().sizeDelta = size;
        }

        MultiplierText.GetComponent<Text>().text = multiplier + "x";

        if (!GameObject.Find(Present.name) && !GameObject.Find(Present.name + "(Clone)"))
        {
            int x = Random.Range(0, 100);
            if (x % 5 == 0) Present = Present1Prefab;
            else if (x % 5 == 1) Present = Present2Prefab;
            else if (x % 5 == 2) Present = Present3Prefab;
            else if (x % 5 == 3) Present = Present4Prefab;
            else if (x % 5 == 4) Present = Present5Prefab;

            Instantiate(Present, new Vector3(0, 7.5f, -12), Quaternion.identity);

            float zPos = Random.Range(-3f, 6f);
            Vector3 Hornpos = new Vector3(-4.24f, 1.45f, zPos);
            HornPrefab.transform.position = Hornpos;
        }

        if (currentScore == nextMultiplierScore && multiplier < 5)
        {
            currentScore = 0;
            multiplier++;
            nextMultiplierScore++;
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            //if (!GameObject.Find("Canvas").GetComponent<PauseMenuScript>().Paused)
                if (Direction.transform.rotation.z < 70f && Direction.transform.rotation.z > -70f)
                    Direction.transform.Rotate(3 * Vector3.forward);
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            //if (!GameObject.Find("Canvas").GetComponent<PauseMenuScript>().Paused)
                if (Direction.transform.rotation.z < 70f && Direction.transform.rotation.z > -70f)
                    Direction.transform.Rotate(3 * Vector3.back);
        }
    }
}
